<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class KategoriHistory extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $fillable = [
        'nama'
    ];
}
