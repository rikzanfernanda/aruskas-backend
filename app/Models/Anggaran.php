<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Anggaran extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $fillable = [
        'users_id', 'nama'
    ];
}
