<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Plan extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $fillable = [
        'users_id', 'anggarans_id',  'bulan', 'tahun', 'jumlah', 'frekuensi', 'satuan', 'total'
    ];
}
