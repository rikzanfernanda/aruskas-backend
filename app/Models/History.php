<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class History extends \Illuminate\Database\Eloquent\Model
{
    use HasFactory;

    protected $fillable = [
        'users_id', 'banks_id', 'anggarans_id', 'kategori_histories_id', 'kegiatan', 'jumlah'
    ];
}
