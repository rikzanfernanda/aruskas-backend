<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Laravel\Lumen\Routing\Controller as BaseController;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\SecurityScheme;

class Controller extends BaseController
{
    protected $response = [];

    public function __construct()
    {
        $this->response = $this->basicResponse();
    }

    private function basicResponse()
    {
        return [
            'timestamp' => Carbon::now()->toDateTimeString(),
            'status' => 200,
            'message' => '',
            'result' => []
        ];
    }

    /**
     * @Info(
     *   title="Aruskas Data API",
     *   version="1.0",
     *   description="This is an API for Aruskas aplication",
     *   @OA\Contact(
     *     email="rikzanfernanda@gmail.com",
     *     name="Rikzan Fernanda"
     *   )
     * )
     */

    /**
     * @SecurityScheme(
     *         securityScheme="bearerAuth",
     *         type="http",
     *         description="Login with email and password to get the authentication token",
     *         scheme="bearer",
     *         bearerFormat="JWT",
     * )
     */

    public function rupiah($n)
    {
        return 'Rp. ' . number_format($n, '0', '',  '.');
    }
}
