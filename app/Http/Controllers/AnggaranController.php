<?php

namespace App\Http\Controllers;

use App\Models\Anggaran;
use App\Models\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class AnggaranController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/anggarans",
     *     summary="Create new anggaran",
     *     tags={"Anggaran"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="multipart/form-data",
     *             @Schema(
     *                 required={"nama"},
     *                 @Property(property="nama", type="string", title="Nama Anggaran", description="Max: 255", example="Uang Makan")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function store(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'nama' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                DB::beginTransaction();

                $anggaran = new Anggaran();
                $anggaran->users_id = auth()->user()->id;
                $anggaran->nama = $request->nama;
                $anggaran->save();

                DB::commit();

                $this->response['message'] = 'Berhasil membuat anggaran baru';
                $this->response['result'] = [
                    "id" => $anggaran->id,
                    "nama" => $anggaran->nama
                ];
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/anggarans",
     *     summary="Get all anggaran by user id",
     *     tags={"Anggaran"},
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-10 09:10:20"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="array",
     *                 @Items(
     *                     @Property(property="id", type="integer", example="1"),
     *                     @Property(property="nama", type="string", example="Uang Makan")
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function index(Request $request)
    {
        try {
            $anggaran = DB::table('anggarans')
                ->where('users_id', auth()->user()->id)
                ->select(['id', 'nama'])
                ->get();

            if ($anggaran) {
                $this->response['result'] = $anggaran;
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'No data found';
            }
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }
        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/anggarans/{id}",
     *     summary="Get anggaran by id",
     *     tags={"Anggaran"},
     *     @Parameter(
     *         name="id",
     *         required=true,
     *         in="path",
     *         @Schema(
     *             type="integer"
     *         )
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-10 09:10:20"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="object",
     *                 @Property(property="id", type="integer", example="1"),
     *                 @Property(property="nama", type="string", example="Uang Makan")
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function show(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(['id' => $id], ['id' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $anggaran = DB::table('anggarans')
                    ->where('id', $id)
                    ->select(['id', 'nama'])
                    ->first();

                if ($anggaran) {
                    $this->response['result'] = $anggaran;
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Put(
     *     path="/api/anggarans/{id}",
     *     summary="Update anggaran by id",
     *     tags={"Anggaran"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="multipart/form-data",
     *             @Schema(
     *                 required={"nama"},
     *                 @Property(property="nama", type="string", title="Nama Anggaran", description="Max: 255", example="Uang Makan")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'nama' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $anggaran = Anggaran::find($id);
                if ($anggaran) {
                    DB::beginTransaction();

                    $anggaran->nama = $request->nama;
                    $anggaran->save();

                    DB::commit();

                    $this->response['message'] = 'Berhasil memperbarui anggaran';
                    $this->response['result'] = [
                        "id" => $anggaran->id,
                        "nama" => $anggaran->nama
                    ];
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Delete(
     *     path="/api/anggarans/{id}",
     *     summary="Delete anggaran by id",
     *     tags={"Anggaran"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function destroy(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(['id' => $id], ['id' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $anggaran = Anggaran::find($id);

                if ($anggaran) {
                    DB::beginTransaction();

                    $histories = DB::table('histories')
                        ->where('anggarans_id', $id)
                        ->count();

                    if ($histories) {
                        History::where('anggarans_id', $id)->update(['anggarans_id' => null]);
                    }
                    $anggaran->delete();

                    DB::commit();

                    $this->response['message'] = 'Berhasil menghapus anggaran';
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }
}
