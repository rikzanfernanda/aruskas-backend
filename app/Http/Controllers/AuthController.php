<?php

namespace App\Http\Controllers;

use App\Mail\ResetPassword;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use OpenApi\Annotations\Parameter;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/register",
     *     tags={"Authentication"},
     *     summary="User registration",
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 required={"nama", "email", "password"},
     *                 @Property(property="nama", title="Nama", type="string", example="Bambang"),
     *                 @Property(property="email", title="Email", type="string", example="bambang@gmail.com"),
     *                 @Property(property="password", title="Password", type="string", format="password"),
     *                 @Property(property="pekerjaan", title="Pekerjaan", type="string", example="Programmer"),
     *                 @Property(property="alamat", title="Alamat", type="string", example="Semarang"),
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error")
     * )
     */
    public function register(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'nama' => 'required|max:100',
            'email' => 'required|unique:users|max:100',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            $user = new User();
            $user->roles_id = 2;
            $user->nama = $request->nama;
            $user->email = $request->email;
            $user->password = app('hash')->make($request->password);
            $user->pekerjaan = $request->pekerjaan;
            $user->alamat = $request->alamat;

            if ($user->save()) {
                $this->response['message'] = 'Data saved successfully!';
                $this->response['result'] = [
                    'id' => $user->id,
                    'nama' => $user->nama,
                    'email' => $user->email,
                    'pekerjaan' => $user->pekerjaan,
                    'alamat' => $user->alamat
                ];
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'Data failed to save!';
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post (
     *     path="/api/login",
     *     summary="User login",
     *     tags={"Authentication"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *                 @Schema (
     *                     required={"email", "password"},
     *                     @Property(property="email", title="Email", type="string", format="email", example="johndoe@gmail.com"),
     *                     @Property(property="password", title="Password", type="string", format="password")
     *                 )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error")
     * )
     */
    public function login(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'email' => 'required|max:100',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $credentials = $request->only('email', 'password');
                $token = Auth::attempt($credentials);

                if ($token) {
                    $this->response['message'] = 'Login successfully!';
                    $this->response['token'] = $token;
                    $this->response['token_type'] = 'bearer';
                    $this->response['id'] = auth()->user()->id;
                    $this->response['nama'] = auth()->user()->nama;
                    $this->response['email'] = auth()->user()->email;
                } else {
                    $this->response['status'] = 422;
                    $this->response['message'] = 'Email or Password is incorrect';
                }
            } catch (\Throwable $throwable) {
                $this->response['status'] = 500;
                $this->response['message'] = $throwable->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post (
     *     path="/api/forgot-password",
     *     summary="Send email to get reset password token",
     *     tags={"Authentication"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema (
     *                 required={"email"},
     *                 @Property(property="email", type="string", description="Email to get link", example="example@gmail.com")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-10 09:10:20"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example="Berhasil request reset password"),
     *             @Property(
     *                 property="result",
     *                 type="object",
     *                 @Property(property="token", type="string", example="QDyXfLhM5MkTZInDk5klfLnGQTuslYFA2aylqjN86o1s5FsBSvHqHGoPXT43fLfH")
     *             )
     *         )
     *     )
     * )
     */
    public function requestResetPassword(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'email' => 'required|max:100|exists:users'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                DB::beginTransaction();

                $token = Str::random(64);
                $password_reset = new PasswordReset();
                $password_reset->email = $request->get('email');
                $password_reset->token = $token;
                $password_reset->save();

                DB::commit();

                $data = [
                    'token' => $token
                ];
                Mail::to($request->get('email'))->send(new ResetPassword($data));
                $this->response['message'] = 'Berhasil request reset password';
                $this->response['result'] = $data;
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post (
     *     path="/api/reset-password",
     *     tags={"Authentication"},
     *     summary="Send new password",
     *     @Parameter (
     *         name="token",
     *         in="query",
     *         required=true,
     *         @Schema (
     *             type="string",
     *             example="QDyXfLhM5MkTZInDk5klfLnGQTuslYFA2aylqjN86o1s5FsBSvHqHGoPXT43fLfH"
     *         )
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema (
     *                 required={"password"},
     *                 @Property(property="password", type="string", format="password", description="New password", example="bambang")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     * )
     */
    public function resetPassword(Request $request)
    {
        $validator = $this->getValidationFactory()->make([
            'token' => $request->get('token'),
            'password' => $request->post('password')
        ], [
            'token' => 'required|exists:password_resets,token',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                DB::beginTransaction();

                $password_reset = PasswordReset::where('token', $request->get('token'))->first();
                if ($password_reset) {
                    $user = User::where('email', $password_reset->email)->first();
                    $user->password = app('hash')->make($request->password);
                    $user->save();

                    $password_reset->delete();

                    $this->response['message'] = "Berhasil reset password";
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = "Gagal reset password";
                }

                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }
}
