<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class UserController extends Controller
{
    /**
     * @Get (
     *     path="/api/users/{id}",
     *     summary="Get user by id",
     *     tags={"User"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-10 09:10:20"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="object",
     *                 @Property(property="id", type="integer", example="1"),
     *                 @Property(property="nama", type="string", example="Bambang"),
     *                 @Property(property="email", type="string", example="bambang@gmail.com"),
     *                 @Property(property="pekerjaan", type="string", example="Wiraswasta"),
     *                 @Property(property="alamat", type="string", example="Semarang")
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */

    public function show($id)
    {
        $validator = $this->getValidationFactory()->make([
            'id' => $id
        ], [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $user = DB::table('users')
                    ->where('id', $id)
                    ->select(['id', 'nama', 'email', 'pekerjaan', 'alamat'])
                    ->first();

                if ($user) {
                    $this->response['result'] = $user;
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @Put (
     *     path="/api/users/{id}",
     *     summary="Update for logged in user",
     *     tags={"User"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 required={"nama", "email"},
     *                 @Property(property="nama", type="string", title="Nama", description="Nama pengguna", example="Bambang"),
     *                 @Property(property="email", type="string", title="Email", description="Email pengguna", example="bambang@gmail.com"),
     *                 @Property(property="pekerjaan", type="string", title="Pekerjaan", description="Pekerjaan pengguna", example="Wiraswasta"),
     *                 @Property(property="alamat", type="string", title="Alamat", description="Alamat pengguna", example="Semarang")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */

    public function update(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'nama' => 'required',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            if ($id == auth()->user()->id) {
                try {
                    $user = User::find($id);
                    if ($user) {
                        DB::beginTransaction();

                        $user->nama = $request->nama;
                        $user->email = $request->email;
                        $user->pekerjaan = $request->pekerjaan;
                        $user->alamat = $request->alamat;
                        $user->save();

                        DB::commit();
                        $this->response['message'] = 'Berhasil memperbarui data user';
                        $this->response['result'] = [
                            'id' => $user->id,
                            'nama' => $user->nama,
                            'email' => $user->email,
                            'pekerjaan' => $user->pekerjaan,
                            'alamat' => $user->alamat
                        ];
                    } else {
                        $this->response['status'] = 500;
                        $this->response['message'] = 'No data found';
                    }
                } catch (\Exception $exception) {
                    DB::rollBack();
                    $this->response['status'] = 500;
                    $this->response['message'] = $exception->getMessage();
                }
            } else {
                $this->response['status'] = 422;
                $this->response['message'] = 'Tidak dapat memperbarui user ini';
            }
        }

        return response()->json($this->response);
    }

    /**
     * @Delete  (
     *     path="/api/users/{id}",
     *     summary="Delete account",
     *     tags={"User"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */

    public function destroy($id)
    {
        $validator = $this->getValidationFactory()->make([
            'id' => $id
        ], [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            if ($id == auth()->user()->id) {
                try {
                    $user = User::find($id);
                    if ($user) {
                        DB::beginTransaction();

                        $user->delete();

                        DB::commit();
                        $this->response['message'] = 'Berhasil menghapus user';
                    } else {
                        $this->response['status'] = 500;
                        $this->response['message'] = 'No data found';
                    }
                } catch (\Exception $exception) {
                    DB::rollBack();
                    $this->response['status'] = 500;
                    $this->response['message'] = $exception->getMessage();
                }
            } else {
                $this->response['status'] = 422;
                $this->response['message'] = 'Tidak dapat menghapus user ini';
            }
        }

        return response()->json($this->response);
    }

    /**
     * @Put (
     *     path="/api/change-password/{id}",
     *     summary="Change password for logged in user",
     *     tags={"User"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 required={"password"},
     *                 @Property(property="password", type="string", title="Password", description="Password baru", example="bambang")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */

    public function changePassword(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            if ($id == auth()->user()->id) {
                try {
                    $user = User::find($id);
                    if ($user) {
                        DB::beginTransaction();

                        $user->password = app('hash')->make($request->password);
                        $user->save();

                        DB::commit();
                        $this->response['message'] = 'Berhasil memperbarui password';
                    } else {
                        $this->response['status'] = 500;
                        $this->response['message'] = 'No data found';
                    }
                } catch (\Exception $exception) {
                    DB::rollBack();
                    $this->response['status'] = 500;
                    $this->response['message'] = $exception->getMessage();
                }
            } else {
                $this->response['status'] = 422;
                $this->response['message'] = 'Tidak dapat memperbarui user ini';
            }
        }

        return response()->json($this->response);
    }
}
