<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class BankController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/banks",
     *     tags={"Bank"},
     *     summary="Get all bank by user_id",
     *     @Response(response="200", description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-01-31 08:22:15"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result", type="array",
     *                 @Items(
     *                     @Property(property="id", type="integer", example="1"),
     *                     @Property(property="nama", type="string", example="BRI"),
     *                     @Property(property="saldo", type="integer", example="1000000")
     *                 )
     *             ),
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function index(Request $request)
    {
        try {
            $banks = DB::table('banks')
                ->where('users_id', auth()->user()->id)
                ->select(['id', 'nama', 'saldo'])
                ->get();

            if ($banks) {
                $this->response['result'] = $banks;
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'No data found';
            }
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/banks",
     *     summary="Create new bank",
     *     tags={"Bank"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="multipart/form-data",
     *             @Schema(
     *                 required={"nama", "saldo"},
     *                 @Property(property="nama", type="string", title="Nama Bank", description="Max: 20", example="BRI"),
     *                 @Property(property="saldo", type="integer", title="Saldo", example="1000000"),
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function store(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'nama' => 'required|max:20',
            'saldo' => 'required'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                DB::beginTransaction();

                $bank = new Bank();
                $bank->users_id = auth()->user()->id;
                $bank->nama = $request->nama;
                $bank->saldo = $request->saldo;
                $bank->save();

                DB::commit();

                $this->response['message'] = 'Berhasil membuat bank baru';
                $this->response['result'] = [
                    "id" => $bank->id,
                    "nama" => $bank->nama,
                    "saldo" => $bank->saldo
                ];
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Put(
     *     path="/api/banks/{id}",
     *     tags={"Bank"},
     *     summary="Update bank",
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         @MediaType(
     *             mediaType="multipart/form-data",
     *             @Schema(
     *                  required={"nama", "saldo"},
     *                  @Property(property="nama", type="string", example="Mandiri"),
     *                  @Property(property="saldo", type="integer", example="500000")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->post(),
            [
                'nama' => 'required',
                'saldo' => 'required'
            ]
        );
        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $bank = Bank::find($id);
                if ($bank) {
                    DB::beginTransaction();

                    $bank->nama = $request->nama;
                    $bank->saldo = $request->saldo;
                    $bank->save();

                    DB::commit();

                    $this->response['message'] = 'Berhasil memperbarui bank';
                    $this->response['result'] = [
                        "id" => $bank->id,
                        "nama" => $bank->nama,
                        "saldo" => $bank->saldo
                    ];
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Delete(
     *     path="/api/banks/{id}",
     *     tags={"Bank"},
     *     summary="Delete bank",
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function destroy(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(
            [
                'id' => $id
            ],
            [
                'id' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $bank = Bank::find($id);
                if ($bank) {
                    DB::beginTransaction();

                    $histories = DB::table('histories')
                        ->where('banks_id', $id)
                        ->count();

                    if ($histories) {
                        History::where('banks_id', $id)->update(['banks_id' => null]);
                    }
                    $bank->delete();

                    DB::commit();

                    $this->response['message'] = 'Berhasil menghapus bank';
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/banks/{id}",
     *     summary="Get bank by id",
     *     tags={"Bank"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-01-31 08:22:15"),
     *             @Property(property="status", type="string", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result", type="object",
     *                 @Property(property="id", type="string", example="16"),
     *                 @Property(property="nama", type="string", example="BRI"),
     *                 @Property(property="saldo", type="integer", example="1000000")
     *             ),
     *         )
     *     ),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function show(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(
            [
                'id' => $id
            ],
            [
                'id' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $bank = DB::table('banks')
                    ->where('id', $id)
                    ->select(['id', 'nama', 'saldo'])
                    ->first();

                if ($bank) {
                    $this->response['result'] = $bank;
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/saldo-bank",
     *     summary="Get jumlah saldo",
     *     tags={"Bank"},
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-07 10:59:30"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(property="result", type="string", example="20000000")
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function getJumlah()
    {
        try {
            $jumlah = DB::table('banks')
                ->where('users_id', auth()->user()->id)
                ->sum('saldo');

            if ($jumlah) {
                $this->response['result'] = intval($jumlah);
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'No data found';
            }
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }
}
