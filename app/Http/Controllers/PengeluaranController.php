<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class PengeluaranController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/pengeluaran",
     *     summary="Create pengeluaran",
     *     tags={"Pengeluaran"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 @Property(
     *                     property="pengeluaran",
     *                     type="array",
     *                     @Items(
     *                         @Property(property="jumlah", type="integer", title="Jumlah", example="500000"),
     *                         @Property(property="bank", type="integer", title="Bank", example="1"),
     *                         @Property(property="anggaran", type="integer", title="Anggaran", example="1")
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function pengeluaran(Request $request)
    {
        try {
            DB::beginTransaction();

            foreach ($request->pengeluaran as $value) {
                $jumlah = $value['jumlah'];
                $banks_id = $value['bank'];
                $anggarans_id = $value['anggaran'];

                $bank = Bank::find($banks_id);

                $bank->saldo = $bank->saldo - $jumlah;
                $bank->save();

                $history = new History();
                $history->users_id = auth()->user()->id;
                $history->banks_id = $banks_id;
                $history->anggarans_id = $anggarans_id;
                $history->kegiatan = "Uang keluar sebesar {$this->rupiah($jumlah)} dari {$bank->nama}";
                $history->kategori_histories_id = 2;
                $history->jumlah = $jumlah;
                $history->save();
            }

            DB::commit();

            $this->response['message'] = 'Berhasil membuat pengeluaran';
        } catch (\Exception $exception) {
            DB::rollBack();

            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/pengeluaran",
     *     summary="Get all pengeluaran by user id",
     *     tags={"Pengeluaran"},
     *     @Parameter(
     *         name="limit",
     *         in="query",
     *         @Schema(type="integer")
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-07 11:37:30"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="array",
     *                 @Items(
     *                     @Property(property="id", type="integer", example="1"),
     *                     @Property(property="banks_id", type="integer", example="1"),
     *                     @Property(property="anggarans_id", type="integer", example="1"),
     *                     @Property(property="kegiatan", type="string", example="Uang keluar sebesar Rp. 1.000 dari BCA"),
     *                     @Property(property="jumlah", type="integer", example="500000"),
     *                     @Property(property="created_at", type="string", format="date-time", example="2022-02-07 11:41:25"),
     *                     @Property(property="restore", type="boolean", example="true")
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function getAll(Request $request)
    {
        try {
            if ($request->get('limit') !== null) {
                $pengeluaran = DB::table('histories')
                    ->where('users_id', auth()->user()->id)
                    ->where('kategori_histories_id', 2)
                    ->select(['id', 'banks_id', 'anggarans_id', 'kegiatan', 'jumlah', 'created_at'])
                    ->orderBy('created_at', 'desc')
                    ->limit($request->get('limit'))
                    ->get();
            } else {
                $pengeluaran = DB::table('histories')
                    ->where('users_id', auth()->user()->id)
                    ->where('kategori_histories_id', 2)
                    ->select(['id', 'banks_id', 'anggarans_id', 'kegiatan', 'jumlah', 'created_at'])
                    ->orderBy('created_at', 'desc')
                    ->limit(100)
                    ->get();
            }

            if ($pengeluaran) {
                $now = time();
                for ($i = 0; $i < count($pengeluaran); $i++) {
                    $start = strtotime($pengeluaran[$i]->created_at);
                    $end = strtotime("+1 day", $start);

                    if ($now >= $start && $now <= $end && $pengeluaran[$i]->banks_id) {
                        $pengeluaran[$i]->restore = true;
                    } else {
                        $pengeluaran[$i]->restore = false;
                    }
                }

                $this->response['result'] = $pengeluaran;
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'No data found';
            }
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }
}
