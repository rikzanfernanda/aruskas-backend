<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\History;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class HistoryController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/histories",
     *     summary="Get history",
     *     tags={"History"},
     *     @Parameter(
     *         in="query",
     *         description="Limitation",
     *         name="limit"
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-07 11:37:30"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="array",
     *                 @Items(
     *                     @Property(property="id", type="integer", example="1"),
     *                     @Property(property="kegiatan", type="string", example="Uang masuk sebesar Rp. 1.000 ke BCA"),
     *                     @Property(property="created_at", type="string", format="date-time", example="2022-02-07 11:41:25")
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{ "bearerAuth": {} }}
     * )
     */
    public function getAll(Request $request)
    {
        $limit = $request->limit ? intval($request->limit): 100;

        try {
            $history = DB::table('histories')
                ->where('users_id', auth()->user()->id)
                ->orderBy('created_at', 'desc')
                ->limit($limit)
                ->select(['id', 'kegiatan', 'kategori_histories_id', 'created_at'])
                ->get();

            if ($history) {
                $this->response['result'] = $history;
            } else {
                $this->response['status'] = 500;
                $this->response['message'] = 'No data found';
            }
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/restore/{id}",
     *     summary="Restore history",
     *     tags={"History"},
     *     @Parameter(
     *         name="id",
     *         description="History id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function restore(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(['id' => $id], ['id' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $history = History::find($id);
                if ($history) {
                    $now = time();
                    $start = strtotime($history->created_at);
                    $end = strtotime("+1 day", $start);

                    if ($now >= $start && $now <= $end) {
                        if ($history->banks_id) {
                            $bank = Bank::find($history->banks_id);

                            if ($bank) {
                                DB::beginTransaction();

                                if ($history->kategori_histories_id == 1) {
                                    $bank->saldo = $bank->saldo - $history->jumlah;
                                } else {
                                    $bank->saldo = $bank->saldo + $history->jumlah;
                                }

                                $bank->save();
                                $history->delete();

                                DB::commit();

                                $this->response['message'] = 'Berhasil mengembalikan data, history sudah terhapus';
                            } else {
                                $this->response['status'] = 500;
                                $this->response['message'] = 'No data found';
                            }
                        } else {
                            $this->response['status'] = 500;
                            $this->response['message'] = 'Bank sudah dihapus, tidak dapat restore';
                        }
                    }
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }

            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $tahun
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/histories/{tahun}",
     *     summary="Get histories perbulan in one year",
     *     tags={"History"},
     *     @Parameter(
     *         name="tahun",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-07 11:37:30"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="object",
     *                 @Property(
     *                     property="pemasukan",
     *                     type="array",
     *                     @Items(
     *                         @Property(property="bulan", type="integer", example="2"),
     *                         @Property(property="jumlah", type="integer", example="1000000")
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function historyPerTahun(Request $request, $tahun)
    {
        $validator = $this->getValidationFactory()->make(['tahun' => $tahun], ['tahun' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $pemasukan = DB::table('histories')
                    ->where('users_id', auth()->user()->id)
                    ->where('kategori_histories_id', 1)
                    ->whereRaw("year(created_at) = {$tahun}")
                    ->selectRaw('month(created_at) as bulan, sum(jumlah) as jumlah')
                    ->groupBy('bulan')
                    ->get();

                $pengeluaran = DB::table('histories')
                    ->where('users_id', auth()->user()->id)
                    ->where('kategori_histories_id', 2)
                    ->whereRaw("year(created_at) = {$tahun}")
                    ->selectRaw('month(created_at) as bulan, sum(jumlah) as jumlah')
                    ->groupBy('bulan')
                    ->get();

                $bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

                $result = [];
                for ($i = 1; $i <= 12; $i++) {
                    foreach ($pemasukan as $masuk) {
                        if ($masuk->bulan === $i) {
                            array_push($result, [
                                'name' => 'Pemasukan',
                                'bulan' => $bulan[$i - 1],
                                'value' => intval($masuk->jumlah)
                            ]);
                        } else {
                            array_push($result, [
                                'name' => 'Pemasukan',
                                'bulan' => $bulan[$i - 1],
                                'value' => 0
                            ]);
                        }
                    }
                    foreach ($pengeluaran as $keluar) {
                        if ($keluar->bulan === $i) {
                            array_push($result, [
                                'name' => 'Pengeluaran',
                                'bulan' => $bulan[$i - 1],
                                'value' => intval($keluar->jumlah)
                            ]);
                        } else {
                            array_push($result, [
                                'name' => 'Pengeluaran',
                                'bulan' => $bulan[$i - 1],
                                'value' => 0
                            ]);
                        }
                    }
                }

                $this->response['result'] = $result;
            } catch (\Exception $exception) {
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }
}
