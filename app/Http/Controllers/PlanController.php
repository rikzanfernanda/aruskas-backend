<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class PlanController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Post(
     *     path="/api/plans",
     *     summary="Create plan",
     *     tags={"Plan"},
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 @Property(property="anggarans_id", type="integer", example="16"),
     *                 @Property(property="bulan", type="integer", example="2", minimum="1", maximum="12"),
     *                 @Property(property="tahun", type="integer", example="2022"),
     *                 @Property(property="jumlah", type="integer", example="1000000"),
     *                 @Property(property="frekuensi", type="integer", example="3"),
     *                 @Property(property="satuan", type="string", example="Hari"),
     *                 @Property(property="total", type="integer", example="2000000")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function store(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'anggarans_id' => 'required|numeric',
            'bulan' => 'required|numeric|min:1|max:12',
            'tahun' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'frekuensi' => 'required|numeric',
            'satuan' => 'required',
            'total' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                DB::beginTransaction();

                $plan = new Plan();
                $plan->users_id = auth()->user()->id;
                $plan->anggarans_id = $request->anggarans_id;
                $plan->bulan = $request->bulan;
                $plan->tahun = $request->tahun;
                $plan->jumlah = $request->jumlah;
                $plan->frekuensi = $request->frekuensi;
                $plan->satuan = $request->satuan;
                $plan->total = $request->total;
                $plan->save();

                DB::commit();

                $this->response['message'] = 'Berhasil membuat plan baru';
                $this->response['result'] = [
                    "id" => $plan->id,
                    "anggarans_id" => $plan->anggarans_id,
                    "bulan" => $plan->bulan,
                    "tahun" => $plan->tahun,
                    "jumlah" => $plan->jumlah,
                    "frekuensi" => $plan->frekuensi,
                    "satuan" => $plan->satuan,
                    "total" => $plan->total
                ];

            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/plans",
     *     summary="Get plans",
     *     tags={"Plan"},
     *     @Parameter(
     *         name="bulan",
     *         in="query",
     *         @Schema(type="integer")
     *     ),
     *     @Parameter(
     *         name="tahun",
     *         in="query",
     *         @Schema(type="integer")
     *     ),
     *     @Response(
     *         response="200",
     *         description="OK",
     *         @JsonContent(
     *             type="object",
     *             @Property(property="timestamp", type="string", format="date-time", example="2022-02-11 11:02:45"),
     *             @Property(property="status", type="integer", example="200"),
     *             @Property(property="message", type="string", example=""),
     *             @Property(
     *                 property="result",
     *                 type="array",
     *                 @Items(
     *                     @Property(property="id", type="integer", example="2"),
     *                     @Property(property="nama", type="string", example="Bensin"),
     *                     @Property(property="bulan", type="integer", example="2", minimum="1", maximum="12"),
     *                     @Property(property="tahun", type="integer", example="2022"),
     *                     @Property(property="jumlah", type="integer", example="100000"),
     *                     @Property(property="frekuensi", type="integer", example="3"),
     *                     @Property(property="satuan", type="string", example="Hari"),
     *                     @Property(property="total", type="integer", example="1000000")
     *                 )
     *             )
     *         )
     *     ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function index(Request $request)
    {
        try {
            if ($request->get('bulan') !== null && $request->get('tahun') !== null) {
                $plans = DB::table('plans')
                    ->leftJoin('anggarans', 'plans.anggarans_id', '=', 'anggarans.id')
                    ->where('plans.users_id', auth()->user()->id)
                    ->where('bulan', $request->get('bulan'))
                    ->where('tahun', $request->get('tahun'))
                    ->select(['plans.id', 'anggarans.nama', 'bulan', 'tahun', 'jumlah', 'frekuensi', 'satuan', 'total'])
                    ->get();
            } else {
                $plans = DB::table('plans')
                    ->leftJoin('anggarans', 'plans.anggarans_id', '=', 'anggarans.id')
                    ->where('plans.users_id', auth()->user()->id)
                    ->select(['plans.id', 'anggarans.nama', 'bulan', 'tahun', 'jumlah', 'frekuensi', 'satuan', 'total'])
                    ->get();
            }

            $this->response['result'] = $plans;
        } catch (\Exception $exception) {
            $this->response['status'] = 500;
            $this->response['message'] = $exception->getMessage();
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Put(
     *     path="/api/plans/{id}",
     *     summary="Update plan by id",
     *     tags={"Plan"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @MediaType(
     *             mediaType="application/json",
     *             @Schema(
     *                 @Property(property="anggarans_id", type="integer", example="16"),
     *                 @Property(property="bulan", type="integer", example="2", minimum="1", maximum="12"),
     *                 @Property(property="tahun", type="integer", example="2022"),
     *                 @Property(property="jumlah", type="integer", example="100000"),
     *                 @Property(property="frekuensi", type="integer", example="1"),
     *                 @Property(property="satuan", type="string", example="Hari"),
     *                 @Property(property="total", type="integer", example="100000")
     *             )
     *         )
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make($request->post(), [
            'anggarans_id' => 'required|numeric',
            'bulan' => 'required|numeric|min:1|max:12',
            'tahun' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'frekuensi' => 'required|numeric',
            'satuan' => 'required',
            'total' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $plan = Plan::find($id);

                if ($plan) {
                    DB::beginTransaction();

                    $plan->anggarans_id = $request->anggarans_id;
                    $plan->bulan = $request->bulan;
                    $plan->tahun = $request->tahun;
                    $plan->jumlah = $request->jumlah;
                    $plan->frekuensi = $request->frekuensi;
                    $plan->satuan = $request->satuan;
                    $plan->total = $request->total;
                    $plan->save();

                    DB::commit();

                    $this->response['message'] = 'Berhasil memperbarui plan';
                    $this->response['result'] = [
                        "id" => $plan->id,
                        "anggarans_id" => $plan->anggarans_id,
                        "bulan" => $plan->bulan,
                        "tahun" => $plan->tahun,
                        "jumlah" => $plan->jumlah,
                        "frekuensi" => $plan->frekuensi,
                        "satuan" => $plan->satuan,
                        "total" => $plan->total
                    ];
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Delete(
     *     path="/api/plans/{id}",
     *     summary="Delete plan by id",
     *     tags={"Plan"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK"),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function destroy(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(['id' => $id], ['id' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $plan = Plan::find($id);

                if ($plan) {
                    DB::beginTransaction();

                    $plan->delete();

                    DB::commit();

                    $this->response['message'] = 'Berhasil menghapus plan';
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                DB::rollBack();

                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @Get(
     *     path="/api/plans/{id}",
     *     summary="Get plan by id",
     *     tags={"Plan"},
     *     @Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @Schema(type="integer")
     *     ),
     *     @Response(response="200", description="OK",
     *          @JsonContent(
     *              type="object",
     *              @Property(property="id", type="integer", description="Plan id", example="1"),
     *              @Property(property="anggarans_id", type="integer", description="Anggaran id", example="1"),
     *              @Property(property="bulan", type="integer", description="Bulan of plan", example="2"),
     *              @Property(property="tahun", type="integer", description="Tahun of plan", example="2022"),
     *              @Property(property="jumlah", type="integer", description="Jumlah rencana anggaran", example="50000"),
     *              @Property(property="frekuensi", type="integer", description="Berapa kali anggaran dikeluarkan", example="1"),
     *              @Property(property="satuan", type="string", description="Berapa kali anggaran dikeluarkan", example="Bulan"),
     *              @Property(property="total", type="integer", description="Total anggaran dalam 1 bulan", example="50000")
     *          )
     *      ),
     *     @Response(response="401", description="Unauthorized"),
     *     @Response(response="422", description="Validation error"),
     *     @Response(response="500", description="Internal server error"),
     *     security={{"bearerAuth": {}}}
     * )
     */
    public function show(Request $request, $id)
    {
        $validator = $this->getValidationFactory()->make(['id' => $id], ['id' => 'required|numeric']);

        if ($validator->fails()) {
            $this->response['status'] = 422;
            $this->response['message'] = $validator->errors();
        } else {
            try {
                $plan = DB::table('plans')
                    ->where('id', $id)
                    ->select(['id', 'anggarans_id', 'bulan', 'tahun', 'jumlah', 'frekuensi', 'satuan', 'total'])
                    ->first();

                if ($plan) {
                    $this->response['result'] = $plan;
                } else {
                    $this->response['status'] = 500;
                    $this->response['message'] = 'No data found';
                }
            } catch (\Exception $exception) {
                $this->response['status'] = 500;
                $this->response['message'] = $exception->getMessage();
            }
        }

        return response()->json($this->response);
    }
}
