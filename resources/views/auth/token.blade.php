<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{env('APP_NAME')}}</title>
</head>
<body>
<div class="container">
    <h1>Reset Password</h1>
    Click the link below <br/>
    <a href="{{env('APP_CLIENT')}}/reset-password?token={{$token}}">{{env('APP_CLIENT')}}/reset-password?token={{$token}}</a>
</div>
</body>
</html>
