<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'roles_id' => 1,
            'nama' => 'Admin',
            'email' => 'admin',
            'password' => app('hash')->make('admin')
        ]);

        User::create([
            'roles_id' => 2,
            'nama' => 'User',
            'email' => 'user',
            'password' => app('hash')->make('user')
        ]);
    }
}
