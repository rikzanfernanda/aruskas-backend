<?php

namespace Database\Seeders;

use App\Models\KategoriHistory;
use Illuminate\Database\Seeder;

class KategoriHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'nama' => 'in'
            ],
            [
                'id' => 2,
                'nama' => 'out'
            ],
        ];

        foreach ($data as $d){
            KategoriHistory::create($d);
        }
    }
}
