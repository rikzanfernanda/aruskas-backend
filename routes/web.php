<?php

/** @var \Laravel\Lumen\Routing\Router $router */


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
//    var_dump(\Illuminate\Support\Str::random(64));
    var_dump(date('Y-m-d H:m:s', strtotime('now')));
    var_dump(date('Y-m-d H:m:s', strtotime('now +1 day')));
    return $router->app->version();
});

$router->get('/key', function () use ($router) {
    return \Illuminate\Support\Str::random(32);
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
    $router->post('forgot-password', 'AuthController@requestResetPassword');
    $router->post('reset-password', 'AuthController@resetPassword');

    $router->group(['middleware' => 'auth'], function () use ($router){
        $router->post('banks', 'BankController@store');
        $router->get('banks', 'BankController@index');
        $router->get('banks/{id}', 'BankController@show');
        $router->put('banks/{id}', 'BankController@update');
        $router->delete('banks/{id}', 'BankController@destroy');
        $router->get('saldo-bank', 'BankController@getJumlah');

        $router->post('anggarans', 'AnggaranController@store');
        $router->get('anggarans', 'AnggaranController@index');
        $router->get('anggarans/{id}', 'AnggaranController@show');
        $router->put('anggarans/{id}', 'AnggaranController@update');
        $router->delete('anggarans/{id}', 'AnggaranController@destroy');

        $router->post('pemasukan', 'PemasukanController@pemasukan');
        $router->get('pemasukan', 'PemasukanController@getAll');

        $router->post('pengeluaran', 'PengeluaranController@pengeluaran');
        $router->get('pengeluaran', 'PengeluaranController@getAll');

        $router->get('histories', 'HistoryController@getAll');
        $router->post('restore/{id}', 'HistoryController@restore');
        $router->get('histories/{tahun}', 'HistoryController@historyPerTahun');

        $router->post('plans', 'PlanController@store');
        $router->get('plans', 'PlanController@index');
        $router->put('plans/{id}', 'PlanController@update');
        $router->delete('plans/{id}', 'PlanController@destroy');
        $router->get('plans/{id}', 'PlanController@show');

        $router->get('users/{id}', 'UserController@show');
        $router->put('users/{id}', 'UserController@update');
        $router->delete('users/{id}', 'UserController@destroy');
        $router->put('change-password/{id}', 'UserController@changePassword');
    });
});
