<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Bank;
use App\Models\Anggaran;
use App\Models\User;

class PengeluaranTest extends TestCase
{
    use DatabaseTransactions;

    private $token = '';
    private $users_id;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_create_pengeluaran()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 20000
        ]);

        $anggaran = Anggaran::create([
            'users_id' => $this->users_id,
            'nama' => 'Makan'
        ]);

        $this->post('/api/pengeluaran', [
            'pengeluaran' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id,
                    'anggaran' => $anggaran->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 10000
        ]);

        $this->seeInDatabase('histories', [
            'banks_id' => $bank->id,
            'kategori_histories_id' => 2,
            'jumlah' => 10000
        ]);
    }

    public function test_get_pengeluaran()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 20000
        ]);

        $anggaran = Anggaran::create([
            'users_id' => $this->users_id,
            'nama' => 'Makan'
        ]);

        $this->post('/api/pengeluaran', [
            'pengeluaran' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id,
                    'anggaran' => $anggaran->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $pengeluaran = $this->get('/api/pengeluaran', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        self::assertArrayHasKey('id', $pengeluaran['result'][0]);

        self::assertEquals([
            'banks_id' => $bank->id,
            'anggarans_id' => $anggaran->id,
            'jumlah' => 10000,
            'restore' => true
        ], [
            'banks_id' => $pengeluaran['result'][0]['banks_id'],
            'anggarans_id' => $pengeluaran['result'][0]['anggarans_id'],
            'jumlah' => $pengeluaran['result'][0]['jumlah'],
            'restore' => $pengeluaran['result'][0]['restore']
        ]);
    }
}
