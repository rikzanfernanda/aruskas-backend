<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    private $users_id;
    private $token;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2,
            'pekerjaan' => 'Mahasiswa',
            'alamat' => 'Semarang'
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_get_user()
    {
        $this->login();

        $user = $this->get('/api/users/' . $this->users_id, [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200,
            'result' => [
                'id' => $this->users_id,
                'nama' => 'Bambang',
                'email' => 'bambang@gmail.com',
                'pekerjaan' => 'Mahasiswa',
                'alamat' => 'Semarang'
            ]
        ]);

        self::assertEquals($this->users_id, $user['result']['id']);
    }

    public function test_update_user()
    {
        $this->login();

        $update = $this->put('/api/users/' . $this->users_id, [
            'nama' => 'Bambang Edit',
            'email' => 'bambangedit@gmail.com',
            'pekerjaan' => 'Wiraswasta',
            'alamat' => 'Pemalang'
        ], [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil memperbarui data user'
        ]);

        $this->seeInDatabase('users', [
            'id' => $this->users_id,
            'nama' => 'Bambang Edit',
            'email' => 'bambangedit@gmail.com'
        ]);

        $this->notSeeInDatabase('users', [
            'id' => $this->users_id,
            'email' => 'bambang@gmail.com'
        ]);

        self::assertEquals('Bambang Edit', $update['result']['nama']);
        self::assertEquals('bambangedit@gmail.com', $update['result']['email']);
    }

    public function test_update_user_failed()
    {
        $this->login();

        $user = User::create([
            'nama' => 'Joko',
            'email' => 'joko@gmail.com',
            'password' => app('hash')->make('joko'),
            'roles_id' => 2,
            'pekerjaan' => 'Mahasiswa',
            'alamat' => 'Semarang'
        ]);

        $this->put('/api/users/' . $user->id, [
            'nama' => 'Bambang Edit',
            'email' => 'bambangedit@gmail.com',
            'pekerjaan' => 'Wiraswasta',
            'alamat' => 'Pemalang'
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 422,
            'message' => 'Tidak dapat memperbarui user ini'
        ]);

        $this->notSeeInDatabase('users', [
            'id' => $this->users_id,
            'nama' => 'Bambang Edit',
            'email' => 'bambangedit@gmail.com'
        ]);
    }

    public function test_delete_user()
    {
        $this->login();

        $this->delete('/api/users/' . $this->users_id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil menghapus user'
        ]);

        $this->notSeeInDatabase('users', [
            'id' => $this->users_id
        ]);
    }

    public function test_change_password()
    {
        $this->login();

        $this->put('/api/change-password/' . $this->users_id, [
            'password' => 'jokowo'
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil memperbarui password'
        ]);
    }

    public function test_change_password_min_length()
    {
        $this->login();

        $this->put('/api/change-password/' . $this->users_id, [
            'password' => 'joko'
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 422,
            'message' => [
                'password' => [
                    'The password must be at least 6 characters.'
                ]
            ]
        ]);
    }
}
