<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Bank;
use App\Models\User;

class PemasukanTest extends TestCase
{
    use DatabaseTransactions;

    private $token = '';
    private $users_id;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_create_pemasukan()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 0
        ]);

        $this->post('/api/pemasukan', [
            'pemasukan' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 10000
        ]);

        $this->seeInDatabase('histories', [
            'banks_id' => $bank->id,
            'kategori_histories_id' => 1,
            'jumlah' => 10000
        ]);
    }

    public function test_get_pemasukan()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 0
        ]);

        $this->post('/api/pemasukan', [
            'pemasukan' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $pemasukan = $this->get('/api/pemasukan', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200,
        ]);

        self::assertArrayHasKey('id', $pemasukan['result'][0]);

        self::assertEquals([
            'banks_id' => $bank->id,
            'jumlah' => 10000,
            'restore' => true
        ], [
            'banks_id' => $pemasukan['result'][0]['banks_id'],
            'jumlah' => $pemasukan['result'][0]['jumlah'],
            'restore' => $pemasukan['result'][0]['restore']
        ]);
    }
}
