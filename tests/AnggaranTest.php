<?php

use App\Models\User;
use \Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Anggaran;

class AnggaranTest extends TestCase
{
    use DatabaseTransactions;

    private $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTY1MTc2MjQ3OSwiZXhwIjoxNjU2OTQ2NDc5LCJuYmYiOjE2NTE3NjI0NzksImp0aSI6IjBDT0N1NkEyTjJYTWpWdmUiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.puyDCmYJjJLoa_6QVVbH5YZQbng3hcdlP-61Qv8gPQg';
    private $users_id;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_get_anggarans()
    {
        $this->login();

        $this->get('/api/anggarans', [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);

        $this->seeJson([
            'status' => 200
        ]);
    }

    public function test_create_anggaran()
    {
        $this->login();

        $this->post('/api/anggarans', [
            'nama' => 'Belanja Harian'
        ], [
            'Authorization' => "bearer " . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil membuat anggaran baru'
        ]);

        $this->seeInDatabase('anggarans', [
            'nama' => 'Belanja Harian'
        ]);
    }

    public function test_get_anggaran_by_id()
    {
        $this->login();

        $anggaran = Anggaran::create([
            'users_id' => 1,
            'nama' => 'Makan'
        ]);

        $this->get('/api/anggarans/' . $anggaran->id, [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'result' => [
                'id' => $anggaran->id,
                'nama' => $anggaran->nama
            ]
        ]);
    }

    public function test_get_anggaran_by_id_not_found()
    {
        $this->login();

        $this->get('/api/anggarans/' . -1, [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 500,
            'result' => [],
            'message' => 'No data found'
        ]);
    }

    public function test_update_anggaran_by_id()
    {
        $this->login();

        $anggaran = Anggaran::create([
            'users_id' => 1,
            'nama' => 'Makan'
        ]);

        $this->put('/api/anggarans/' . $anggaran->id, [
            'nama' => 'Minum'
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil memperbarui anggaran'
        ]);

        $this->notSeeInDatabase('anggarans', [
            'id' => $anggaran->id,
            'nama' => $anggaran->nama
        ]);

        $this->seeInDatabase('anggarans', [
            'id' => $anggaran->id,
            'nama' => 'Minum'
        ]);
    }

    public function test_destroy_anggaran_by_id()
    {
        $this->login();

        $anggaran = Anggaran::create([
            'users_id' => 1,
            'nama' => 'Makan'
        ]);

        $this->delete('/api/anggarans/' . $anggaran->id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil menghapus anggaran'
        ]);

        $this->notSeeInDatabase('anggarans', [
            'id' => $anggaran->id
        ]);
    }
}
