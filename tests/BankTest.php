<?php


use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Bank;

class BankTest extends TestCase
{
    use DatabaseTransactions;

    private $users_id;
    private $token = '';

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_get_banks()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $this->get('/api/banks', [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200,
            'result' => [
                [
                    'id' => $bank->id,
                    'nama' => $bank->nama,
                    'saldo' => $bank->saldo
                ]
            ]
        ]);
    }

    public function test_create_bank()
    {
        $this->login();

        $this->post('/api/banks', [
            'nama' => 'BRI',
            'saldo' => 50000
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200
        ]);
        $this->seeInDatabase('banks', [
            'nama' => 'BRI',
            'saldo' => 50000
        ]);
    }

    public function test_edit_bank()
    {
        $this->login();

        $create = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $this->put('/api/banks/' . $create->id, [
            'nama' => 'BRI EDIT',
            'saldo' => 100000
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil memperbarui bank'
        ]);
        $this->seeInDatabase('banks', [
            'id' => $create->id,
            'nama' => 'BRI EDIT',
            'saldo' => 100000
        ]);
    }

    public function test_delete_bank()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $this->delete('/api/banks/' . $bank->id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200,
            'message' => 'Berhasil menghapus bank'
        ]);
        $this->notSeeInDatabase('banks', [
            'id' => $bank->id
        ]);
    }

    public function test_get_bank_by_id()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $this->get('/api/banks/' . $bank->id, [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200,
            'result' => [
                'id' => $bank->id,
                'nama' => $bank->nama,
                'saldo' => $bank->saldo
            ]
        ]);
    }

    public function test_get_jumlah_bank()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $bank1 = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 10000
        ]);

        $this->get('/api/saldo-bank', [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'status' => 200,
            'result' => $bank->saldo + $bank1->saldo
        ]);
    }
}
