<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Anggaran;
use App\Models\Plan;

class PlanTest extends TestCase
{
    use DatabaseTransactions;

    private $token = '';
    private $users_id;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_get_all_plan()
    {
        $this->login();

        $plan = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 50000,
            'frekuensi' => 1,
            'satuan' => 'bulan',
            'total' => 50000
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id
        ]);

        $plans = $this->get('/api/plans', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        $expected = [
            'id' => $plan->id
        ];

        $actual = [
            'id' => $plans['result'][0]['id']
        ];

        self::assertEquals($expected, $actual);

        self::assertArrayHasKey('id', $plans['result'][0]);
        self::assertArrayHasKey('nama', $plans['result'][0]);
        self::assertArrayHasKey('bulan', $plans['result'][0]);
        self::assertArrayHasKey('tahun', $plans['result'][0]);
        self::assertArrayHasKey('jumlah', $plans['result'][0]);
        self::assertArrayHasKey('frekuensi', $plans['result'][0]);
        self::assertArrayHasKey('satuan', $plans['result'][0]);
        self::assertArrayHasKey('total', $plans['result'][0]);
    }

    public function test_get_plan_by_bulan_dan_tahun()
    {
        $this->login();

        $plan = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 6,
            'tahun' => 2023,
            'jumlah' => 50000,
            'frekuensi' => 1,
            'satuan' => 'bulan',
            'total' => 50000
        ]);

        $plan2 = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 50000,
            'frekuensi' => 1,
            'satuan' => 'bulan',
            'total' => 50000
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan2->id
        ]);

        $plans = $this->get('/api/plans?bulan=6&tahun=2023', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        self::assertEquals($plan->id, $plans['result'][0]['id']);
        self::assertNotEquals($plan2->id, $plans['result'][0]['id']);
    }

    public function test_create_plan()
    {
        $this->login();

        $this->post('/api/plans', [
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->seeInDatabase('plans', [
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ]);
    }

    public function test_get_plan_by_id()
    {
        $this->login();

        $plan = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id
        ]);

        $getPlan = $this->get('/api/plans/' . $plan->id, [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        self::assertArrayHasKey('id', $getPlan['result']);
        self::assertArrayHasKey('anggarans_id', $getPlan['result']);
        self::assertArrayHasKey('bulan', $getPlan['result']);
        self::assertArrayHasKey('tahun', $getPlan['result']);
        self::assertArrayHasKey('jumlah', $getPlan['result']);
        self::assertArrayHasKey('frekuensi', $getPlan['result']);
        self::assertArrayHasKey('satuan', $getPlan['result']);
        self::assertArrayHasKey('total', $getPlan['result']);

        self::assertEquals($plan->id, $getPlan['result']['id']);
    }

    public function test_update_plan()
    {
        $this->login();

        $plan = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id
        ]);

        $this->put('/api/plans/' . $plan->id, [
            'anggarans_id' => 2,
            'bulan' => 6,
            'tahun' => 2023,
            'jumlah' => 20000,
            'frekuensi' => 1,
            'satuan' => 'minggu',
            'total' => 80000
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id,
            'users_id' => $this->users_id,
            'anggarans_id' => 2,
            'bulan' => 6,
            'tahun' => 2023,
            'jumlah' => 20000,
            'frekuensi' => 1,
            'satuan' => 'minggu',
            'total' => 80000
        ]);

        $this->notSeeInDatabase('plans', [
            'id' => $plan->id,
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ]);
    }

    public function test_destroy_plan()
    {
        $this->login();

        $plan = Plan::create([
            'users_id' => $this->users_id,
            'anggarans_id' => 1,
            'bulan' => 5,
            'tahun' => 2022,
            'jumlah' => 10000,
            'frekuensi' => 4,
            'satuan' => 'bulan',
            'total' => 40000
        ]);

        $this->seeInDatabase('plans', [
            'id' => $plan->id
        ]);

        $this->delete('/api/plans/' . $plan->id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->notSeeInDatabase('plans', [
            'id' => $plan->id
        ]);
    }
}
