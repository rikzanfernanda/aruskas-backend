<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Bank;
use App\Models\History;
use App\Models\Anggaran;

class HistoryTest extends TestCase
{
    use DatabaseTransactions;

    private $token = '';
    private $users_id;

    private function login()
    {
        User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        // login
        $login = $this->post('/api/login', [
            'email' => 'bambang@gmail.com',
            'password' => 'bambang'
        ])->response->decodeResponseJson();

        $this->token = $login['token'];
        $this->users_id = $login['id'];
    }

    public function test_get_histories()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 0
        ]);

        $this->post('/api/pemasukan', [
            'pemasukan' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $histories = $this->get('/api/histories', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeInDatabase('histories', [
            'banks_id' => $bank->id,
            'kategori_histories_id' => 1,
            'jumlah' => 10000
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $expected = 'Uang masuk sebesar Rp. 10.000 ke BRI';

        $actual = $histories['result'][0]['kegiatan'];

        self::assertEquals($expected, $actual);
    }

    public function test_restore_pemasukan()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 0
        ]);

        $this->post('/api/pemasukan', [
            'pemasukan' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 10000
        ]);

        $history = History::where('users_id', $this->users_id)
            ->where('banks_id', $bank->id)
            ->where('jumlah', 10000)
            ->first();

        $this->post('/api/restore/' . $history->id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->notSeeInDatabase('histories', [
            'id' => $history->id
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 0
        ]);
    }

    public function test_restore_pengeluaran()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 20000
        ]);

        $anggaran = Anggaran::create([
            'nama' => 'Belanja',
            'users_id' => $this->users_id
        ]);

        $this->post('/api/pengeluaran', [
            'pengeluaran' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id,
                    'anggaran' => $anggaran->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 10000
        ]);

        $history = History::where('users_id', $this->users_id)
            ->where('banks_id', $bank->id)
            ->where('jumlah', 10000)
            ->first();

        $this->post('/api/restore/' . $history->id, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        $this->notSeeInDatabase('histories', [
            'id' => $history->id
        ]);

        $this->seeInDatabase('banks', [
            'id' => $bank->id,
            'saldo' => 20000
        ]);
    }

    public function test_restore_history_id_not_found()
    {
        $this->login();

        $this->post('/api/restore/' . -1, [], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $this->seeJson([
            'status' => 500,
            'message' => 'No data found'
        ]);
    }

    public function test_get_Histories_per_tahun()
    {
        $this->login();

        $bank = Bank::create([
            'users_id' => $this->users_id,
            'nama' => 'BRI',
            'saldo' => 0
        ]);

        $this->post('/api/pemasukan', [
            'pemasukan' => [
                [
                    'jumlah' => 50000,
                    'bank' => $bank->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $anggaran = Anggaran::create([
            'nama' => 'Belanja',
            'users_id' => $this->users_id
        ]);

        $this->post('/api/pengeluaran', [
            'pengeluaran' => [
                [
                    'jumlah' => 10000,
                    'bank' => $bank->id,
                    'anggaran' => $anggaran->id
                ]
            ]
        ], [
            'Authorization' => 'bearer ' . $this->token
        ]);

        $histories = $this->get('/api/histories/2022', [
            'Authorization' => 'bearer ' . $this->token
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        $expected = [
            'pemasukan' => [
                'bulan' => 5,
                'jumlah' => 50000
            ],
            'pengeluaran' => [
                'bulan' => 5,
                'jumlah' => 10000
            ]
        ];

        $actual = [
            'pemasukan' => $histories['result']['pemasukan'][0],
            'pengeluaran' => $histories['result']['pengeluaran'][0],
        ];

        self::assertEquals($expected, $actual);
    }
}
