<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function test_register_failed()
    {
        $this->json('POST', '/api/register', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
            'password' => 'andi',
            'pekerjaan' => '',
            'alamat' => ''
        ]);

        $cek = $this->json('POST', '/api/register', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
            'password' => 'andi',
            'pekerjaan' => '',
            'alamat' => ''
        ]);

        $cek->seeStatusCode(200);
        $cek->seeJson([
            'status' => 422
        ]);
    }

    public function test_register_success()
    {
        $post = $this->json('POST', '/api/register', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
            'password' => 'andi',
            'pekerjaan' => '',
            'alamat' => ''
        ]);

        $post->seeJson([
            'status' => 200
        ]);
        $post->seeInDatabase('users', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
        ]);
    }

    public function test_login_failed()
    {
        $this->json('POST', '/api/register', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
            'password' => 'andi',
            'pekerjaan' => '',
            'alamat' => ''
        ]);

        $cek = $this->json('POST', '/api/login', [
            'email' => 'andi@gmail.com',
            'password' => 'andin'
        ]);

        $cek->seeJson([
            'status' => 422,
            'message' => 'Email or Password is incorrect'
        ]);
    }

    public function test_login_success()
    {
        $this->json('POST', '/api/register', [
            'nama' => "Andi",
            'email' => 'andi@gmail.com',
            'password' => 'andi',
            'pekerjaan' => '',
            'alamat' => ''
        ]);

        $cek = $this->json('POST', '/api/login', [
            'email' => 'andi@gmail.com',
            'password' => 'andi'
        ]);

        $cek->seeJson([
            'status' => 200,
            'message' => 'Login successfully!'
        ]);
    }

    public function test_request_reset_password()
    {
        $user = User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        $req = $this->post('/api/forgot-password', [
            'email' => $user->email
        ])->response->decodeResponseJson();

        $this->seeJson([
            'status' => 200
        ]);

        $this->seeInDatabase('password_resets', [
            'email' => $user->email,
            'token' => $req['result']['token']
        ]);
    }

    public function test_reset_password()
    {
        $user = User::create([
            'nama' => 'Bambang',
            'email' => 'bambang@gmail.com',
            'password' => app('hash')->make('bambang'),
            'roles_id' => 2
        ]);

        $req = $this->post('/api/forgot-password', [
            'email' => $user->email
        ])->response->decodeResponseJson();

        $token = $req['result']['token'];

        $reset = $this->post('/api/reset-password?token=' . $token, [
            'password' => 'dua'
        ])->response->decodeResponseJson();

        $this->seeInDatabase('password_resets', [
            'email' => $user->email,
            'token' => $req['result']['token']
        ]);

        $this->seeJson([
            'status' => 200
        ]);

        self::assertEquals('Berhasil reset password', $reset['message']);
    }
}
